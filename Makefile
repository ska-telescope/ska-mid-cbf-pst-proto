# define private overrides for above variables in here
-include PrivateRules.mak

# include makefile to pick up the standard Make targets, e.g., 'make build'
# build, 'make push' docker push procedure, etc. The other Make targets
# ('make interactive', 'make test', etc.) are defined in this file.
#
include .make/*.mk


#PYTHON_VARS_BEFORE_PYTEST = PYTHONPATH=./images/ska-mid-cbf-signal-verification

#PYTHON_VARS_AFTER_PYTEST = -m "not post_deployment"

# PYTHON_BUILD_TYPE = non_tag_setup

PYTHON_SWITCHES_FOR_FLAKE8 = --ignore=DAR201

#PYTHON_SWITCHES_FOR_PYLINT = --disable=W0613,C0116,C0114,R0801,W0621,C0301,F0010

PYTHON_LINT_TARGET = ./src/

documentation:  ## Re-generate documentation
	cd docs && make clean && make html

help: ## show this help
	@echo "make targets:"
	@grep -E '^[0-9a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ": .*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

requirements:
	poetry install

.PHONY: all documentation help

