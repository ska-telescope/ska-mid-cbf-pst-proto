#!/usr/bin/env python3

"""Define function placeholders and test function examples."""


# TBD: Replace all following code with desired functionality for the package
def function_example():
    """Make a function outside of a class."""


# TBD: Replace all following code with desired functionality for package
class Sample:
    """Define class, methods etc."""

    something = 0

    def example(self):
        """Define non return function for subsequent test."""

    def example_2(self):
        """
        Define function with specific return value.

        Returns:
          int: value 2
        """
        return 2
