Mid.CBF AA1 PST Beamforming
================================

Mid.CBF PST Beamforming for AA1 is the simplest form of beamforming.
The individual beams are only added together, there is no additional
manipulation of the signal.

Mid.CBF is commanded by LMC and provides it's output products to the PST element.

.. figure:: ./diagrams/csp-cbf-pst.png
    :align: center

    CBF PST External Interfaces

Focus for Design
------------------
* Define the complete set of parameters flowing throughout Mid CBF
* Drive well defined layers

  * Drive parameters and interactions down to the lowest possible level in the software
  * MCS interacts only with VCC and FSP Top Level Devices
  * MCS manages interactions between talon boards

    * required for correlation of more than 1 200 MHz frequency slice
    * required for PST Beamforming?

  * VCC and FSP top level devices are only aware of interactions between low level device servers
  * Low level device servers store all parameters, calculations etc that are used only by that device server, do not push this higher in the software

* Prefer anything that can be done in the software, be done in the software rather than in the firmware
* FW design includes definition of required parameters, calculations etc needed to configure and control the firmware; this forms the interface between the FW and the SW tango device servers


Scenarios
-------------

AA1
****
* One subarray, one receptor, one fs, one talon board
* One subarray, two receptors, one fs, one talon board
* One subarray, one receptor, two fs, two talon boards
* One subarray, two receptors, three fs, three talon boards
* Add more

Beyond AA1
***********
* Two subarray etc.

Design References
----------------------
* *311-000000-003 SKA1 CSP Mid Correlator and Beamformer Subelement Detailed Design Document (EB-4a) Rev 3 2018-10-11*
* ICDs for LMC to CBF, PSS, PST: `Mid.CBF Interface Control Documents (ICDs) <https://confluence.skatelescope.org/pages/viewpage.action?pageId=218403431>`_
* `Mid CBF AA1 PST Software <https://confluence.skatelescope.org/display/SE/Mid+CBF+AA1+PST+Software>`_
* `Mid CBF AA1 PST Firmware <https://confluence.skatelescope.org/display/SE/Mid+CBF+AA1+PST+Firmware>`_


Source repository for this documentation: `ska-mid-cbf-pst-proto <https://gitlab.com/ska-telescope/ska-mid-cbf-pst-proto>`_.