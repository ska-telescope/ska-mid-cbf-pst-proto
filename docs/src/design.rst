
HPS
====

Test Vector Generation
*************************
The *Test Vector Generator* enables more direct testing of the beamformer (beam summer) by injecting well-defined test signals into the PST Sync Buffer, by-passing the preceding components.
See the `pst_processing.vhd diagram <https://confluence.skatelescope.org/display/SE/Mid+CBF+AA1+PST+Firmware#MidCBFAA1PSTFirmware-pst_processing.vhd>`_ for details.

.. uml:: /diagrams/test-vector-generation-hsp-fsp.puml
