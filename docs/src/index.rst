.. ska-mid-cbf-pst-proto documentation master file
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

SKA Mid CBF PST Beamforming Prototype Repo
===========================================================

.. toctree::
   :maxdepth: 2
   :caption: Home
   :hidden:

.. OVERVIEW ===============================================================
.. toctree::
   :maxdepth: 2
   :caption: Intro to AA1 PST Beamforming

   introduction

.. DESIGN =============================================================
.. toctree::
   :maxdepth: 2
   :caption: Design

   design

.. toctree::
   :maxdepth: 2
   :caption: HPS Low Level Devices

   hps-low-level-devices

.. SLIM ===================================================================
.. toctree::
   :maxdepth: 2
   :caption: SLIM

   slim

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
