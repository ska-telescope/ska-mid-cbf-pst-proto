# SKA Mid.CBF PST Beamforming Prototype Repo

[ReadTheDocs](https://ska-telescope-ska-mid-cbf-pst-proto.readthedocs.io/en/latest/)

## Description
This repository is a collection of code and documentation to support design
for PST Beamforming for the SKA Mid.CBF. It is a place to test out ideas,
and document design. It supplements other design artifacts such as those
on Confluence at [Mid CBF PST FW](https://confluence.skatelescope.org/display/SE/Mid+CBF+AA1+PST+Firmware) and
[Mid CBF PST SW](https://confluence.skatelescope.org/display/SE/Mid+CBF+AA1+PST+Software)

## Installation
```bash
git clone https://gitlab.com/ska-telescope/ska-mid-cbf-pst-proto.git
cd ska-mid-cbf-pst-proto
git submodule init
git submodule update
poetry install # to create the virtual environment with all dependencies
poetry shell # to run shell in the virtual environment
make python-build # or "poetry run make python-build" if not in the poetry shell
```
